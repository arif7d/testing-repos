<?php return array(
	'admin' => array(
		'agent.general.*',
		'agent.tickets.*',
		'adm.general.*',
		'adm.departments.*',
		'adm.tickets.*',
	),

	'reports' => array(
		'agent.general.*',
		'agent.tickets.*',
		'adm.general.*',
		'adm.departments.*',
		'adm.tickets.*'
	)
);