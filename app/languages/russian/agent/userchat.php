<?php return array(
	'agent.userchat.agent_blocked_user_on'                                 => '{{name}} установил(а) блокировку до {{date}}',
	'agent.userchat.assigned_to'                                           => 'Чаты назначены на {{name}}',
	'agent.userchat.block_ip_address'                                      => 'Заблокировать IP-адрес',
	'agent.userchat.block_ip_address_info'                                 => 'Блокировка IP-адресов может быть эффективной, но в результате этого могут пострадать пользователи публичных IP-адресов (например, в публичных сетях).',
	'agent.userchat.block_reason'                                          => 'Причина: {{reason}}',
	'agent.userchat.block_title'                                           => 'Завершить чат и запретить пользователю создавать чаты в ближайшие 24 часа.',
	'agent.userchat.block_user'                                            => 'Заблокировать пользователя',
	'agent.userchat.create_ticket'                                         => 'Создать тикет',
	'agent.userchat.ended_by'                                              => '{{name}} завершил(а) чат',
	'agent.userchat.ended_by_reason'                                       => '{{name}} завершил(а) чат по причине: {{reason}}',
	'agent.userchat.ended_user'                                            => 'Чат завершен пользователем',
	'agent.userchat.full_message'                                          => 'Полное сообщение',
	'agent.userchat.ident_unconfirmed'                                     => 'Идентификация не завершена, e-mail не подтвержден',
	'agent.userchat.list_title'                                            => '1 чат|{{count}} чат(ов)',
	'agent.userchat.message_agent-timeout'                                 => '{{name}} не отвечает. Подождите, пока будет подобран другой агент.',
	'agent.userchat.message_assigned'                                      => 'Чаты назначены на {{name}}',
	'agent.userchat.message_ended'                                         => 'Чат завершен',
	'agent.userchat.message_ended-by'                                      => '{{name}} завершил(а) чат',
	'agent.userchat.message_ended-by-user'                                 => 'Чат завершен пользователем',
	'agent.userchat.message_set-department'                                => '{{name}} установил(а) подразделение {{department}}',
	'agent.userchat.message_set_department'                                => '{{name}} установил(а) подразделение {{department}}',
	'agent.userchat.message_started'                                       => 'Чат начат',
	'agent.userchat.message_unassigned'                                    => 'Чат не назначен',
	'agent.userchat.message_uploading'                                     => 'Загрузка...',
	'agent.userchat.message_user-joined'                                   => '{{name}} вошел в чат',
	'agent.userchat.message_user-left'                                     => '{{name}} покинул чат',
	'agent.userchat.message_user-returned'                                 => 'Пользователь вернулся',
	'agent.userchat.message_user-timeout'                                  => 'Пользователь отсоединен',
	'agent.userchat.message_user_joined'                                   => '{{name}} вошел в чат',
	'agent.userchat.message_user_left'                                     => '{{name}} покинул чат',
	'agent.userchat.message_wait-timeout'                                  => 'Чат завершен: не удалось найти агента',
	'agent.userchat.msg_agent_timeout'                                     => '{{name}} не отвечает. Пожалуйста, подождите, пока будет найдет доступный агент.',
	'agent.userchat.msg_new_user_track'                                    => 'Пользователь просматривает: {{label}}',
	'agent.userchat.msg_user_timeout'                                      => 'Пользователь не отвечает.',
	'agent.userchat.new_chat'                                              => 'Новый чат',
	'agent.userchat.take_chat'                                             => 'Принять чат',
	'agent.userchat.taken_by'                                              => 'Чат принял',
	'agent.userchat.transcript_sent'                                       => 'История сообщений отправлена на {{email}}',
	'agent.userchat.unassigned'                                            => 'Чат не назначен',
	'agent.userchat.unblock'                                               => 'Разблокировать',
	'agent.userchat.user_is_blocked'                                       => 'Пользователь заблокирован',
	'agent.userchat.visitor_info'                                          => 'Информация для посетителей',
	'agent.userchat.you_have_been_invited'                                 => 'Вы были приглашены в чат',
);
