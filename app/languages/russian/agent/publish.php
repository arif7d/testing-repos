<?php return array(
	'agent.publish.add_new_category'                                       => 'Добавить новую категорию',
	'agent.publish.add_new_glossary'                                       => 'Добавить новое слово в словарь',
	'agent.publish.add_pending'                                            => 'Добавить статью для рассмотрения',
	'agent.publish.all_articles'                                           => 'Все статьи',
	'agent.publish.all_comments'                                           => 'Все комментарии',
	'agent.publish.all_downloads'                                          => 'Все загрузки',
	'agent.publish.all_draft_content'                                      => 'Все черновики',
	'agent.publish.all_news'                                               => 'Все новости',
	'agent.publish.apply_action'                                           => 'Применить действие',
	'agent.publish.article_comments'                                       => 'Комментарии к статье',
	'agent.publish.compare_selected'                                       => 'Сравнить выбранное',
	'agent.publish.edit_new_glossary'                                      => 'Редактировать словарь',
);
