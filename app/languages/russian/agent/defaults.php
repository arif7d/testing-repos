<?php return array(
	'agent.defaults.default_style'                                         => 'Стиль по умолчанию',
	'agent.defaults.team_lvl1_support'                                     => '1-я линия поддержки',
	'agent.defaults.team_lvl2_support'                                     => '2-я линия поддержки',
	'agent.defaults.team_support_managers'                                 => 'Менеджеры поддержки',
	'agent.defaults.usergroup_agent_all_non_destructive'                   => 'Все безопасные разрешения',
	'agent.defaults.usergroup_agent_all_non_destructive_note'              => 'Агент обладает всеми возможностями, но не может вносить критические изменения, такие как удаление записей.',
	'agent.defaults.usergroup_agent_all_perms'                             => 'Все разрешения',
	'agent.defaults.usergroup_agent_all_perms_note'                        => 'Агент обладает всеми возможностями.',
	'agent.defaults.usergroup_everyone'                                    => 'Все',
	'agent.defaults.usergroup_everyone_note'                               => 'Каждый пользователь, включая гостей и зарегистрированных участников.',
	'agent.defaults.usergroup_registered'                                  => 'Зарегистрированные',
	'agent.defaults.usergroup_registered_note'                             => 'Все участники, зарегистрированные в системе.',
);
