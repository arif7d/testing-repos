<?php return array(
	'agent.people.im_account_placeholder'                                  => 'Имя пользователя аккаунта',
	'agent.people.phone_country_placeholder'                               => 'Страна',
	'agent.people.phone_number_placeholder'                                => 'Номер телефона',
	'agent.people.website_url_placeholder'                                 => 'URL сайта',
);
