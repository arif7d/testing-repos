<?php return array(
	'adm.ticket_escalations.escalations'       => 'Escalations',
	'adm.ticket_escalations.delete_confirm'    => 'Are you sure you want to delete this escalations? Any actions this escalation used to perform will no longer work.',
	'adm.ticket_escalations.title_explain'     => 'This title will be used throughout the admin interface to refer to this escalation.',
	'adm.ticket_escalations.no_escalations'    => 'You have not created any escalations yet.',
	'adm.ticket_escalations.count_escalations' => '{{count}} Escalation|{{count}} Escalations',
	'adm.ticket_escalations.new_escalation'    => 'New Escalation',
);