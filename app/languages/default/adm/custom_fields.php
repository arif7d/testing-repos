<?php return array(
	'adm.custom_fields.title_error'       => 'You must enter a valid title',
	'adm.custom_fields.description_error' => 'You must enter a valid description',
);