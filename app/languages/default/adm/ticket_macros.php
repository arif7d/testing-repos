<?php return array(
	'adm.ticket_macros.macros'         => 'Macros',
	'adm.ticket_macros.delete_confirm' => 'Are you sure you want to delete this macro?',
	'adm.ticket_macros.title_explain'  => 'This title will be displayed in the agent interface in the Macro list.',
	'adm.ticket_macros.no_macros'      => 'You have not created any macros yet.',
	'adm.ticket_macros.count_macros'   => '{{count}} Macro|{{count}} Macros',
);