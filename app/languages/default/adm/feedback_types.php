<?php return array(
	'adm.feedback_types.no_delete_last'               => 'At least one feedback type must always exist. You cannot delete the last feedback type.',
	'adm.feedback_types.title_explain'                => 'This is the title as it will appear throughout the agent and user interfaces.',
	'adm.feedback_types.title_error'                  => 'You must enter a valid title',
	'adm.feedback_types.title'                        => 'Title',
	'adm.feedback_types.delete_feedback_type'         => 'Delete Feedback Type',
	'adm.feedback_types.delete_feedback_type_confirm' => 'Are you sure you want to delete this feedback type?',
	'adm.feedback_types.delete_feedback_types_moved'  => 'Any feedback that is currently under this feedback type will be moved to:',
	'adm.feedback_types.usergroups'                   => 'Usergroups',
	'adm.feedback_types.usergroups_explain'           => 'These are usergroups to which this feedback type belongs.',
);