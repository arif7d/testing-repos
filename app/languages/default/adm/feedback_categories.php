<?php return array(
	'adm.feedback_categories.title_explain'                    => 'This is the title as it will appear throughout the agent and user interfaces.',
	'adm.feedback_categories.title_error'                      => 'You must enter a valid title',
	'adm.feedback_categories.title'                            => 'Title',
	'adm.feedback_categories.delete_feedback_category'         => 'Delete Feedback Category',
	'adm.feedback_categories.delete_feedback_category_confirm' => 'Are you sure you want to delete this feedback category?',
	'adm.feedback_categories.delete_feedback_categories_moved' => 'Any feedback that is currently under this feedback category will be moved to:',
	'adm.feedback_categories.new_category'                     => 'New Feedback Category',
	'adm.feedback_categories.parent_explain'                   => 'By setting a parent category, this category becomes a child. This is just an organizational feature that helps you create feedback structures that are easier to use.',
);