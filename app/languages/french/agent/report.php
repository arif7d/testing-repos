<?php return array(
	'agent.report.agent_activity'                                          => 'Activité agent',
	'agent.report.agent_hours'                                             => 'Heures agent',
	'agent.report.clone'                                                   => 'Clone',
	'agent.report.clone_bracket'                                           => '[Clone]',
	'agent.report.current'                                                 => 'Actuel',
	'agent.report.data_range'                                              => 'Série de données',
	'agent.report.overview'                                                => 'Aperçu',
	'agent.report.report_builder'                                          => 'Editeur de rapport',
	'agent.report.updates'                                                 => 'Mises à jour',
);
