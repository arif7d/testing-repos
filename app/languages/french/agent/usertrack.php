<?php return array(
	'agent.usertrack.first_visit_date'                                     => 'Date de la première visite',
	'agent.usertrack.initial_landing_page'                                 => 'Page de renvoi',
	'agent.usertrack.initial_visit_time'                                   => 'Début de la visite',
	'agent.usertrack.landing_pages'                                        => 'Pages de renvoi',
	'agent.usertrack.number_of_pages'                                      => 'Nombre de pages',
	'agent.usertrack.number_of_visits'                                     => 'Nombre de visites',
	'agent.usertrack.session_landing_page'                                 => 'Page de renvoi de la session',
	'agent.usertrack.session_start_time'                                   => 'Début de la session',
	'agent.usertrack.visited_pages'                                        => 'Pages visitées',
	'agent.usertrack.visitor_id'                                           => 'ID visiteur',
);
