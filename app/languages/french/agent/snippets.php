<?php return array(
	'agent.snippets.add_snippet'                                           => 'Ajouter un snippet',
	'agent.snippets.all_snippets'                                          => 'Tous les snippets',
	'agent.snippets.cat_can_be_used_by'                                    => 'La catégorie peut être utilisée par :',
	'agent.snippets.chat_snippets'                                         => 'Snippets du chat',
	'agent.snippets.edit_snippet'                                          => 'Éditer le snippet',
	'agent.snippets.insert_variable'                                       => 'Insérer une variable',
	'agent.snippets.learn_more_about_vars'                                 => 'En savoir plus sur les variables',
	'agent.snippets.must_create_cat'                                       => 'Avant de commencer à créer des snippets, <br/> il faut créer votre première catégorie.',
	'agent.snippets.shortcut_code'                                         => 'Code du raccourci',
	'agent.snippets.shortcut_code_info'                                    => 'Entrer ce code dans la réponse du ticket pour insérer automatiquement le snippet. Par exemple, avec le code raccourci de \'réponse standard\', vous tapez simplement "%standard-response%\' pour insérer le snippet sans avoir à ouvrir le gestionnaire de snippets.',
	'agent.snippets.ticket_snippets'                                       => 'Snippets de ticket',
);
