<?php return array(
	'agent.defaults.default_style'                                         => 'Style par défaut',
	'agent.defaults.team_lvl1_support'                                     => '1er niveau d\'assistance',
	'agent.defaults.team_lvl2_support'                                     => '2ème niveau d\'assistance',
	'agent.defaults.team_support_managers'                                 => 'Coordinateurs',
	'agent.defaults.usergroup_agent_all_non_destructive'                   => 'Toutes les permissions non destructrices',
	'agent.defaults.usergroup_agent_all_non_destructive_note'              => 'L\'agent a toutes les permissions sauf celles qui lui permettraient  de faire des changements destructeurs comme supprimer des éléments.',
	'agent.defaults.usergroup_agent_all_perms'                             => 'Toutes les permissions',
	'agent.defaults.usergroup_agent_all_perms_note'                        => 'L\'agent a toutes les permissions',
	'agent.defaults.usergroup_everyone'                                    => 'Chacun',
	'agent.defaults.usergroup_everyone_note'                               => 'Tous les utilisateurs y compris les membres invités et enregistrés.',
	'agent.defaults.usergroup_registered'                                  => 'Enregistré',
	'agent.defaults.usergroup_registered_note'                             => 'Toutes les personnes enregistrées dans le système',
);
