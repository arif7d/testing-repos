<?php return array(
	'user.defaults.article_category_general'                               => '일반',
	'user.defaults.article_example_content'                                => '이것은 지식 베이스 글의 예문입니다. 에이전트 인터페이스에서 수정이나 삭제하셔도 좋습니다.',
	'user.defaults.article_example_title'                                  => '예시 논문',
	'user.defaults.department_sales'                                       => '세일즈',
	'user.defaults.department_support'                                     => '지원',
	'user.defaults.download_category_general'                              => '일반',
	'user.defaults.downloads_category_general'                             => '문서들',
	'user.defaults.feedback_example_content'                               => '이것은 예시 제안입니다. 에이전트 인터페이스에서 수정이나 삭제하셔도 좋습니다.',
	'user.defaults.feedback_example_title'                                 => '예시 제안',
	'user.defaults.feedback_status_completed'                              => '완료',
	'user.defaults.feedback_status_declined'                               => '감소되었다',
	'user.defaults.feedback_status_duplicate'                              => '사본을 만들다',
	'user.defaults.feedback_status_planning'                               => '계획',
	'user.defaults.feedback_status_started'                                => '시작되었습니다',
	'user.defaults.feedback_status_under-review'                           => '검토중입니다',
	'user.defaults.feedback_type_bug-report'                               => '버그를 신고합니다',
	'user.defaults.feedback_type_feature-request'                          => '특집 요청',
	'user.defaults.feedback_type_suggestion'                               => '제안',
	'user.defaults.language_english'                                       => '영어',
	'user.defaults.news_category_general'                                  => '일반',
	'user.defaults.news_example_content'                                   => '이것은 예시 뉴스 게시물입니다. 에이전트 인터페이스에서 수정이나 삭제하셔도 좋습니다',
	'user.defaults.news_example_title'                                     => '예시 뉴스 게시물',
	'user.defaults.trigger_warn_autoclose'                                 => '티켓에 3일동안 답을 하지 않으셨습니다. 문제가 해결되셨으면 이 메세지에 답변을 하지 않으셔도 되고 티켓은 이틀안에 해결됩니다. 더 도움이 필요하시다면 이 메세지에 답변을 하시고 티켓을 열어놔주십시오.',
);
