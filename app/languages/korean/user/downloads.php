<?php return array(
	'user.downloads.button-download'                                       => '다운로드',
	'user.downloads.change_file'                                           => '파일 바꾸기',
	'user.downloads.downloads-count'                                       => '이 파일은 한번 다운로드 되었습니다 이 파일은 {{count}} 번 다운로드 되었습니다',
	'user.downloads.file_not_found'                                        => '파일을 찾을 수 없습니다',
	'user.downloads.message_no-files'                                      => '이 폴더에는 파일이 없습니다',
	'user.downloads.noun'                                                  => '파일',
	'user.downloads.related_downloads'                                     => '관련 파일들',
	'user.downloads.sidebar_description'                                   => '자료보기 | 등록된 파일 {{count}} 개',
	'user.downloads.title'                                                 => '다운로드 파일들',
	'user.downloads.title-categories'                                      => '보조 폴더 1개가 있습니다 보조 폴더 {{count}} 가 있습니다',
	'user.downloads.title-downloads'                                       => '한개의 파일이 있습니다. {{count}}개의 파일이 있습니다.',
);
