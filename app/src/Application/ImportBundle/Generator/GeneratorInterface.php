<?php

namespace Application\ImportBundle\Generator;

/**
 * Description of GeneratorInterface
 *
 * @author Abhinav Kumar <abhinav.kumar@deskpro.com>
 */
interface GeneratorInterface
{
	public function generateJson();
}
