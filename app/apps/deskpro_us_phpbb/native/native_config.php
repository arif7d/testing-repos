<?php return array(
	'install' => array(
		'handler' => 'deskpro_us_phpbb\\InstallerHandler'
	),
	'api' => array(
		'package_request_handler' => 'deskpro_us_phpbb\\RequestHandler\\PackageRequestHandler'
	)
);