<?php

return array(
	'id' => 1,
	'sent_to_address' => '',
	'email_account_address' => '',
	'creation_system' => 'web.person',
	'creation_system_option' => '',
	'ticket_hash' => '561b59a759d44542e3cfd3484d44dd3d7693043a',
	'status' => 'awaiting_agent',
	'hidden_status' => NULL,
	'validating' => NULL,
	'is_hold' => false,
	'urgency' => 1,
	'count_agent_replies' => 1,
	'count_user_replies' => 0,
	'feedback_rating' => NULL,
	'date_feedback_rating' => NULL,
	'date_feedback_rating_ts' => 0,
	'date_feedback_rating_ts_ms' => 0,
	//'date_created' => '2014-04-03 11:51:13',
	//'date_created_ts' => 1396525873,
	//'date_created_ts_ms' => 1396525873000,
	'date_resolved' => NULL,
	'date_resolved_ts' => 0,
	'date_resolved_ts_ms' => 0,
	'date_closed' => NULL,
	'date_closed_ts' => 0,
	'date_closed_ts_ms' => 0,
	'date_first_agent_assign' => NULL,
	'date_first_agent_assign_ts' => 0,
	'date_first_agent_assign_ts_ms' => 0,
	'date_first_agent_reply' => '2014-04-03 11:51:13',
	'date_first_agent_reply_ts' => 1396525873,
	'date_first_agent_reply_ts_ms' => 1396525873000,
	'date_last_agent_reply' => '2014-04-03 11:51:13',
	'date_last_agent_reply_ts' => 1396525873,
	'date_last_agent_reply_ts_ms' => 1396525873000,
	'date_last_user_reply' => NULL,
	'date_last_user_reply_ts' => 0,
	'date_last_user_reply_ts_ms' => 0,
	'date_agent_waiting' => NULL,
	'date_agent_waiting_ts' => 0,
	'date_agent_waiting_ts_ms' => 0,
	'date_user_waiting' => '2014-04-03 11:51:13',
	'date_user_waiting_ts' => 1396525873,
	'date_user_waiting_ts_ms' => 1396525873000,
	'date_status' => '2014-04-03 11:51:13',
	'date_status_ts' => 1396525873,
	'date_status_ts_ms' => 1396525873000,
	'total_user_waiting' => 0,
	'total_to_first_reply' => 0,
	'date_locked' => NULL,
	'date_locked_ts' => 0,
	'date_locked_ts_ms' => 0,
	'has_attachments' => false,
	'subject' => 'Test',
	'original_subject' => 'Test',
	'properties' => NULL,
	'worst_sla_status' => NULL,
	'waiting_times' =>
		array(),
	'parent_ticket' => NULL,
	'language' => NULL,
	'department' =>
		array(
			'id' => 1,
			'title' => 'Support',
			'user_title' => '',
			'is_tickets_enabled' => true,
			'is_chat_enabled' => false,
			'display_order' => 0,
			'title_full' => 'Support',
			'parent_id' => NULL,
			'parent_ids' =>
				array(),
			'title_parts' =>
				array(
					0 => 'Support',
				),
			'user_title_parts' =>
				array(
					0 => 'Support',
				),
			'has_children' => false,
		),
	'category' => NULL,
	'priority' => NULL,
	'workflow' => NULL,
	'product' => NULL,
	'person' =>
		array(
			'id' => 1,
			'gravatar_url' => 'http://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?&d=mm',
			'disable_picture' => false,
			'is_contact' => true,
			'is_user' => true,
			'is_agent' => true,
			'was_agent' => false,
			'can_agent' => true,
			'can_admin' => true,
			'can_billing' => true,
			'can_reports' => true,
			'is_vacation_mode' => false,
			'disable_autoresponses' => false,
			'disable_autoresponses_log' => '',
			'is_confirmed' => true,
			'is_agent_confirmed' => true,
			'is_deleted' => false,
			'is_disabled' => false,
			'importance' => 0,
			'creation_system' => 'web.person',
			'name' => 'Admin Admin',
			'first_name' => 'Admin',
			'last_name' => 'Admin',
			'title_prefix' => '',
			'override_display_name' => '',
			'summary' => '',
			'organization_position' => '',
			'organization_manager' => false,
			'timezone' => 'UTC',
			'date_created' => '2014-04-03 11:51:12',
			'date_created_ts' => 1396525872,
			'date_created_ts_ms' => 1396525872000,
			'date_last_login' => NULL,
			'date_last_login_ts' => 0,
			'date_last_login_ts_ms' => 0,
			'date_picture_check' => NULL,
			'date_picture_check_ts' => 0,
			'date_picture_check_ts_ms' => 0,
			'picture_blob' => null,
			'organization' => NULL,
			'emails' =>
				array(
					0 =>
						array(
							'id' => 1,
							'email' => 'admin@example.com',
						),
				),
			'custom_data' =>
				array(),
			'contact_data' =>
				array(),
			'usergroups' =>
				array(
					0 =>
						array(
							'id' => 3,
							'title' => 'All Permissions',
							'note' => '',
							'is_agent_group' => true,
							'sys_name' => NULL,
							'is_enabled' => true,
						),
				),
			'labels' =>
				array(),
			'display_name' => 'Admin Admin',
			'primary_email' =>
				array(
					'id' => 1,
					'email' => 'admin@example.com',
				),
			'usergroup_ids' =>
				array(),
			'agentgroup_ids' =>
				array(
					0 => 3,
				),
			'picture_url' => 'http://localhost:8888/file.php/avatar/80/default.jpg?size-fit=1',
			'picture_url_80' => 'http://localhost:8888/file.php/avatar/80/default.jpg?size-fit=1',
			'picture_url_64' => 'http://localhost:8888/file.php/avatar/64/default.jpg?size-fit=1',
			'picture_url_50' => 'http://localhost:8888/file.php/avatar/50/default.jpg?size-fit=1',
			'picture_url_45' => 'http://localhost:8888/file.php/avatar/45/default.jpg?size-fit=1',
			'picture_url_32' => 'http://localhost:8888/file.php/avatar/32/default.jpg?size-fit=1',
			'picture_url_22' => 'http://localhost:8888/file.php/avatar/22/default.jpg?size-fit=1',
			'picture_url_16' => 'http://localhost:8888/file.php/avatar/16/default.jpg?size-fit=1',
			'default_picture_url' => 'http://localhost:8888/file.php/avatar/80/default.jpg?size-fit=1',
			'default_picture_url_80' => 'http://localhost:8888/file.php/avatar/80/default.jpg?size-fit=1',
			'default_picture_url_64' => 'http://localhost:8888/file.php/avatar/64/default.jpg?size-fit=1',
			'default_picture_url_50' => 'http://localhost:8888/file.php/avatar/50/default.jpg?size-fit=1',
			'default_picture_url_45' => 'http://localhost:8888/file.php/avatar/45/default.jpg?size-fit=1',
			'default_picture_url_32' => 'http://localhost:8888/file.php/avatar/32/default.jpg?size-fit=1',
			'default_picture_url_22' => 'http://localhost:8888/file.php/avatar/22/default.jpg?size-fit=1',
			'default_picture_url_16' => 'http://localhost:8888/file.php/avatar/16/default.jpg?size-fit=1',
		),
	'person_email' =>
		array(
			'id' => 1,
			'email' => 'admin@example.com',
			'email_domain' => 'example.com',
			'is_own_validated' => false,
			'is_validated' => true,
			'comment' => '',
			'date_created' => '2014-04-03 11:51:13',
			'date_created_ts' => 1396525872,
			'date_created_ts_ms' => 1396525872000,
			'date_validated' => '2014-04-03 11:51:13',
			'date_validated_ts' => 1396525872,
			'date_validated_ts_ms' => 1396525872000,
		),
	'person_email_validating' => NULL,
	'agent' => NULL,
	'agent_team' => NULL,
	'organization' => NULL,
	'custom_data' =>
		array(),
	'participants' =>
		array(),
	'charges' =>
		array(),
	'ticket_slas' =>
		array(),
	'labels' =>
		array()
);